from typing import List

from model import Transaction
from ofxtools.models.bank.stmt import STMTTRN
from ofxtools.models.ofx import OFX
from ofxtools.Parser import OFXTree

StrList = List[str]


def parse_file(filepath: str) -> OFX:
    parser = OFXTree()
    with open(filepath, "rb") as f:
        parser.parse(f)

    return parser.convert()


def map_transaction(transaction: STMTTRN) -> Transaction:
    return Transaction(transaction.trntype, transaction.dtposted,
                       transaction.trnamt, transaction.name, transaction.memo)


def map_transactions(transactions: List[STMTTRN]) -> List[object]:
    return [map_transaction(transaction) for transaction in transactions]


def parseCreditCardFile(filepath: str) -> StrList:
    tree = parse_file(filepath)
    transactions = tree.creditcardmsgsrsv1[0].ccstmtrs.banktranlist

    return map_transactions(transactions)
