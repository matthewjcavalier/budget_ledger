from qfx_parser.parser import parseCreditCardFile

if __name__ == "__main__":
    resource_path = "../res/export.qfx"
    [print(tr) for tr in parseCreditCardFile(resource_path)]
