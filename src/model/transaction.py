import datetime
from dataclasses import dataclass


@dataclass
class Transaction:
    """Class to represent money transaction"""
    type: str
    posted: datetime
    amount: float
    account_name: str
    memo: str
