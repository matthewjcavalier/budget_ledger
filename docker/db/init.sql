CREATE USER pyUser;

CREATE SCHEMA budget_ledger AUTHORIZATION pyUser;

CREATE TABLE IF NOT EXISTS budget_ledger.users (
  user_id serial PRIMARY KEY,
  username varchar(50) UNIQUE NOT NULL,
  password varchar(255) NOT NULL,
  email varchar(255),
  created_on TIMESTAMP NOT NULL
);

INSERT INTO budget_ledger.users
            (username, password, email, created_on)
VALUES ('testUser1', 'abc123', 'test@test.com', NOW());

CREATE TABLE IF NOT EXISTS budget_ledger.accounts (
  account_id serial PRIMARY KEY,
  account_name varchar(100),
  owned_by INT,
  CONSTRAINT fk_owner
  FOREIGN KEY(owned_by)
  REFERENCES budget_ledger.users(user_id)
);

INSERT INTO budget_ledger.accounts (account_name, owned_by)
VALUES ('Credit Card', 1);

CREATE TABLE IF NOT EXISTS budget_ledger.transactions (
  transaction_id serial PRIMARY KEY,
  type varchar(50),
  posted timestamp,
  amount NUMERIC(11, 2),
  memo varchar(255),
  account_id INT,
  CONSTRAINT fk_account
  FOREIGN KEY(account_id)
  REFERENCES budget_ledger.accounts(account_id)
);

INSERT INTO budget_ledger.transactions (type, posted, amount, memo, account_id)
VALUES ('debit', NOW(), -100, 'Bought BIG Coffee', 1);

INSERT INTO budget_ledger.transactions (type, posted, amount, memo, account_id)
VALUES ('credit', NOW(), 55.50, 'Bought BIG Coffee', 1);

